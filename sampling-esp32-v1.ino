#define SAMPPER 1 //sampling period in miliseconds

int sensorPin = 32;    // select the input pin for the potentiometer
int sensorValue = 0;

volatile int interruptCounter;

//unsigned long StartTime = 0;

hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
}

void setup() {
  Serial.begin(115200);

  // señal con frecuencia de 1 Mhz, se le aplica un preescaler de 80 porque la frecuenia del reloj interno es de 80MHz
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  // Cada 10000 edges se lanza el interrupt, es decir, cada 10 mili segundos
  timerAlarmWrite(timer, SAMPPER*1000, true);
  timerAlarmEnable(timer);
}

void loop() {

  if (interruptCounter > 0) {
    portENTER_CRITICAL(&timerMux);
    interruptCounter--;
    portEXIT_CRITICAL(&timerMux);

    // read the value from the sensor:
    sensorValue = analogRead(sensorPin);

    //unsigned long CurrentTime = micros();
    //unsigned long ElapsedTime = CurrentTime - StartTime;
    //StartTime = CurrentTime;

    //Serial.println(ElapsedTime);
    Serial.println(sensorValue);
  }

  

}
